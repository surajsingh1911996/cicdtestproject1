# Use an appropriate base image
FROM python:3.8-slim

# Set work directory
WORKDIR /usr/src/app

# Install python3-venv if not included
RUN apt-get update && apt-get install -y python3-venv

# Create and activate virtual environment
RUN python3 -m venv /venv
ENV PATH="/venv/bin:$PATH"

# Copy your requirements.txt and install Python dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of your application
COPY . .

# Command to run your application
CMD ["python", "run.py"]
